package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    List<Student> students;
    List<Student> students1;
    public StudentDaoImpl(){
        students = new ArrayList<>();
        students.add(new Student("123","A","temp",2.33));
        students.add(new Student("456","B","temp",4.00));
        students.add(new Student("789","C","temp",2.81));
        students.add(new Student("101","D","temp",3.12));
        students.add(new Student("555","E","temp",3.89));

        students1 = new ArrayList<>();
        students1.add(new Student("111","F","x",4.00));
        students1.add(new Student("222","G","x",4.00));
        students1.add(new Student("333","H","x",4.00));
        students1.add(new Student("444","I","x",4.00));
        students1.add(new Student("555","J","x",4.00));

    }

    @Override
    public List<Student> findAll() {
        return this.students;
    }

    @Override
    public List<Student> find() {
        return this.students1;
    }

}
