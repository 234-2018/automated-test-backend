package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.entity.Student;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class StudentServiceImplTest {
    StudentDao studentDao;
    StudentServiceImpl studentService;
    @Before
    public void setup(){
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
    }

    @Test
    public void testFindById(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
        assertThat(studentService.findStudentById("456"),is(new Student("456","B","temp",4.00)));
        assertThat(studentService.findStudentById("789"),is(new Student("789","C","temp",2.81)));
        assertThat(studentService.findStudentById("101"),is(new Student("101","D","temp",3.12)));
    }

    @Test
    public void testgetAverageGpa(){
        StudentServiceImpl studentService = new StudentServiceImpl();
        StudentDaoImpl studentDao = new StudentDaoImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(),is(closeTo(3.23,0.01)));
    }


    @Test
    public void testgetAverageGpaStudents1(){
        StudentServiceImpl studentService = new StudentServiceImpl();
        StudentDaoImpl studentDao = new StudentDaoImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpaStudents1(),is(closeTo(4.00,0.03)));
    }

    @Test
    public void testWithMock(){

        List<Student> mockStudents = new ArrayList<>();

        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));

    }

    @Test
    public void testStudents1WithMock(){
        StudentDao studentDao = mock (StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("456","B","temp",4.00));
        mockStudents.add(new Student("789","C","temp",2.81));
        mockStudents.add(new Student("101","D","temp",3.12));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("789"),is(new Student("789","C","temp",2.81)));

    }

    @Test
    public void testgetAvereageGPAWithMock(){
        StudentDao studentDao = mock (StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",4.00));
        mockStudents.add(new Student("456","B","temp",4.00));
        mockStudents.add(new Student("789","C","temp",4.00));
        mockStudents.add(new Student("101","D","temp",4.00));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(closeTo(4.00,0.01)));

    }

    @Test
    public void testFindByPartOfId(){
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("456","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        mockStudents.add(new Student("112","E","temp",2.33));
        mockStudents.add(new Student("114","F","temp",2.33));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("22")
                ,hasItem(new Student("223","C","temp",2.33)));
        assertThat(studentService.findStudentByPartOfId("22")
                ,hasItems(new Student("223","C","temp",2.33)
                ,new Student("224","D","temp",2.33)));

        assertThat(studentService.findStudentByPartOfId("11")
                ,hasItems(new Student("112","E","temp",2.33)
                        ,new Student("114","F","temp",2.33)));
    }


//    @Test(expected = ArithmeticException.class)
//    public void testNoDataException(){
//        List<Student> mockStudents = new ArrayList<>();
//        mockStudents.add(new Student("123","A","temp",2.33));
//
//        when(studentDao.findAll()).thenReturn(mockStudents);
//        assertThat(studentService.getAverageGpa(),is(closeTo(4.00,0.1)));
//    }


}
